<!-- BEGIN FOOTER-->
    
    <p id="back-top">
		<a href="#top"><span></span></a>
	</p>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<p class="pull-left">
						 &copy; 2014. Webmation LLC. All Rights Reserved.
					</p>
				</div>
				<div class="col-md-5">
					<ul class="footermenu pull-right">
						<li><a href="index.php">Home</a></li>
						<li><a href="features.php">Features</a></li>
                        <li><a href="#">Partners</a></li>
						<li><a href="http://webmation.net/clients/twotest/blog">Blog</a></li>
						<li><a href="#">Legal</a></li>
						<li><a href="contact-us.php">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!-- /footer section end-->
</div>
<!-- /.wrapbox ends-->
<!-- SCRIPTS, placed at the end of the document so the pages load faster
================================================== -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript"> 
$(document).ready(function () {
// ---- FAQs ---------------------------------------------------------------------------------------------------------------
$('.faqs dd').hide(); // Hide all DDs inside .faqs
$('.faqs dt').hover(function(){$(this).addClass('hover')},function(){$(this).removeClass('hover')}).click(function(){ // Add class "hover" on dt when hover
$(this).next().slideToggle('normal'); // Toggle dd when the respective dt is clicked
}); 
});
</script>
</body>
</html>