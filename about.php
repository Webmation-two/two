<?php include 'header.php'; ?>

	<!-- PAGE TITLE
================================================== -->
	<section class="pageheader-default-about pageheader-default text-center">
    <div class="semitransparentbg">
		<h1 class="animated fadeInLeftBig notransition" style="font-weight: 100;">About TakeWebOrders</h1>
		<p class="animated fadeInRightBig notransition container page-description">
			Dedicated to your restaurant's success.<br />
		</p>
	</div>
	</section>
	<div class="wrapsemibox">
		<div class="semiboxshadow text-center">
			<img src="img/shp.png" class="img-responsive" alt="">
		</div>
		<!-- SERVICES
================================================== -->
	<section class="home-features" style="padding-top: 20px;">
		<div class="container">
				<h1 class="small text-center">CONNECT WITH US</h1>
		<div class="row col-md-12">
        <div class="col-md-3 text-center">
            <h4>&nbsp;</h4>
        </div> 
        <div class="col-md-6 col-sm-12 col-xs-12 text-center" style="text-align:center;">
            <ul class="social-icons-large list-soc">
							<li class="social-icons-large-facebook"><a href="http://facebook.com/takeweborders" target="_blank"><i class="icon-facebook"></i></a></li>
							<li class="social-icons-large-twitter"><a href="http://twitter.com/takeweborders" target="_blank"><i class="icon-twitter"></i></a></li>
							<li class="social-icons-large-linkedin"><a href="http://linkedin.com/company/4982663?trk=prof-exp-company-name" target="_blank"><i class="icon-linkedin"></i></a></li>
							<li class="social-icons-large-google"><a href="https://plus.google.com/+Takeweborders/posts" target="_blank"><i class="icon-google-plus"></i></a></li>
							<!--li><a href="#"><i class="icon-skype"></i></a></li-->
			</ul>
        </div>
        
        <div class="col-md-3 text-center">
            <h4>&nbsp;</h4>
        </div>
    </div>
                    <!--div class="br-hr type_short">
						<span class="br-hr-h">
						<i class="icon-pencil">Test</i>
						</span>
					</div-->
	
		</div>
        <div class="grayarea row center-block text-justify">
        <h1 class="small text-center">OUR PHILOSOPHY</h1>
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <p>At TakeWebOrders our goal is help your restaurant succeed and create a long term relationship with your business. We want to foster the direct relationship between you and your customers while being your silent technology partners. Our parent company Webmation has been in business since 2001, has an A+ BBB rating with most of our clients staying with us for over a decade for one simple reason. We want our clients to prosper by creating a win-win situation that sees us both profit. We are not a startup that will be gone in a few months and we are not backed by VC funding, so we don't have to answer to anyone but you. Your restaurant is our first and only priority.
        </p>
        </div>
        <div class="col-md-2"></div>
		</section>	
        <!-- TEAM
================================================== -->
		<section class="container notransition topspace10 col-md-12 col-sm-12">
		<div class="row" class="text-center">
			<!--h1 class="text-center smalltitle">Our Team
			</h1-->
            <h3 style="font-weight:300; text-transform:capitalize; padding: 15px;text-align:center;">Real people. Here when you need us.</h3>
			<div class="col-md-3 col-sm-6" >
				<div class="thumbnail">
					<img style="width: 100%; max-width:238px; height:auto;" src="img/team/hersh.jpg" alt="">
					<div class="caption">
						<h4>Hersh Sandhoo</h4>
						<span class="primarycol">President & CEO</span>
                        <br />
						<!--p>
							 Praesent id metus ante, ut condimentum magna. Nam bibendum, felis eget.<br>
						</p-->
                        <div class="row">
						<ul class="social-icons" style="padding-top: 15px; margin-left:-10px;">
							<!--li><a href="#"><i class="icon-facebook"></i></a></li-->
                            <div class="col-md-12 col-sm-12 text-center">
							<li><a href="http://twitter.com/webmation"><i class="icon-twitter"></i></a></li>
							<li><a href="http://linkedin.com/in/webmation" target="_blank"><i class="icon-linkedin"></i></a></li>
                            </div>
							<!--li><a href="#"><i class="icon-google-plus"></i></a></li>
							<li><a href="#"><i class="icon-pinterest"></i></a></li-->
						</ul></div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="thumbnail"> 
					<img src="img/team/sara.jpg" alt="">
					<div class="caption">
						<h4>Sara Llanes</h4>
						<span class="primarycol">VP of Marketing</span>
                        <br />
						<!--p>
							 Praesent id metus ante, ut condimentum magna. Nam bibendum, felis eget.<br>
						</p-->
						<ul class="social-icons" style="padding-top: 15px;">
							<!--li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li-->
							<li><a href="http://linkedin.com/in/sarallanes" target="_blank"><i class="icon-linkedin"></i></a></li>
							<!--li><a href="#"><i class="icon-google-plus"></i></a></li>
							<li><a href="#"><i class="icon-pinterest"></i></a></li-->
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="thumbnail">
					<img src="img/team/enrique.jpg" alt="">
					<div class="caption">
						<h4>Enrique Somoza</h4>
						<span class="primarycol">VP of Operations</span>
                        <br />
						<!--p>
							 Praesent id metus ante, ut condimentum magna. Nam bibendum, felis eget.<br>
						</p-->
						<ul class="social-icons" style="padding-top: 15px;">
							<!--li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li-->
							<li><a href="http://linkedin.com/in/enriquesomoza" target="_blank"><i class="icon-linkedin"></i></a></li>
							<!--li><a href="#"><i class="icon-google-plus"></i></a></li>
							<li><a href="#"><i class="icon-pinterest"></i></a></li-->
						</ul>
					</div>
				</div>
			</div>
            <div class="col-md-3 col-sm-6">
				<div class="thumbnail">
					<img src="img/team/shikha.jpg" alt="">
					<div class="caption">
						<h4>Shikha Chadha</h4>
						<span class="primarycol">VP of Customer Service</span>
                        <br />
						<!--p>
							 Praesent id metus ante, ut condimentum magna. Nam bibendum, felis eget.<br>
						</p-->
						<ul class="social-icons" style="padding-top: 15px;">
							<!--li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li-->
							<li><a href="http://linkedin.com/" target="_blank"><i class="icon-linkedin"></i></a></li>
							<!--li><a href="#"><i class="icon-google-plus"></i></a></li>
							<li><a href="#"><i class="icon-pinterest"></i></a></li-->
						</ul>
					</div>
				</div>
			</div>
		</div>
		</section>
	
		<!--section class="container services">
		<div class="row text-justify animated fadeInUp notransition">
        <h1 class="text-left smalltitle" style="font-weight:100;">
			<span>Media & Press</span>
			</h1>
			<div class="col-md-4">
            <div class="text-center">
				<i class="icon-desktop"></i>
				<h4>FORBES</h4>
                </div>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.<a href="#"> Read more</a>
				</p>
			</div>
			<!-- col-md-4 
			<div class="col-md-4">
            <div class="text-center">
				<i class="icon-globe"></i>
				<h4>WIRED</h4>
                </div>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.<a href="#"> Read more</a>
				</p>
			</div>
			<!-- col-md-4 
			<div class="col-md-4">
	            <div class="text-center">
				<i class="icon-trophy"></i>
				<h4>TECHCRUNCH</h4>
                </div>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.<a href="#"> Read more</a>
				</p>
			</div>
			<!-- col-md-4 
		</div-->
		<!--div class="row text-center topspace30 animated fadeInDown notransition">
			<!--div class="col-md-4">
				<i class="icon-heart"></i>
				<h4>SOCIAL MEDIA</h4>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
				</p>
			</div-->
			<!-- col-md-4 -->
			<!--div class="col-md-4">
				<i class="icon-shopping-cart"></i>
				<h4>E-COMMERCE</h4>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
				</p>
			</div-->
			<!-- col-md-4 -->
			<!--div class="col-md-4">
				<i class="icon-cloud"></i>
				<h4>CLOUD SERVICES</h4>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.
				</p>
			</div-->
			<!-- col-md-4 
		</div-->
		</section>
		<!-- CLIENTS
================================================== -->
		
		<section class="grayarea recent-projects-home text-center">
		<div class="container">
			<div class="row col-md-12" style="padding-bottom: 30px;">
				<h1 class="small text-center notransition">Become a Partner</h1>
            </div>
            <div class="row col-md-12">
            <div class="row text-justify col-md-6" style="padding-top:20px;">
                <p>Share TakeWebOrders with your restaurant clients and contacts. We have developed an amazing partner program that pays ongoing residual commissions on all plans.</p>
                <p>We have special partner programs for ISOs, Independent Sales Representatives, Sales Teams, and Restaurant Industry Partners.</p>
                <p>Apply today to receive more information.</p>
                <p class="notransition topspace20 text-center">
					 <input onClick="parent.location='http://www.takeweborders.com/join.php'" type="submit" id="submit" class="btn btn-large btn-twoblue col-md-5 text-center" value="APPLY NOW" />
				</p>
                </div>
                <div class="row col-md-6">
                <!--div id="cbp-qtrotator" class="cbp-qtrotator">
					<div class="cbp-qtcontent"-->
						<!--img style="margin:0 auto" src="img/usms_logo_white.png" alt=""-->
						<blockquote class="thin">
							<p class="smquote">
								<i class="icon-quote-left colortext quoteicon"></i><span style="text-align:left;"> Our agents and staff are excited to off TakeWebOrders to our restaurant clients. We pride ourselves on exceeding our customers' expectations and we have not seen a company give this much value in the 22 years we have been in business. Our agents are reporting that TakeWebOrders is the easiest sale they have ever made.</span>
							</p>
							<footer>Stu Rosenbaum, CEO, US Merchant Systems</footer>
						</blockquote>
					</div>
                <!--p>"Our agents and staff are excited to off TakeWebOrders to our restaurant clients. We pride ourselves on exceeding our customers' expectations and we have not seen a company give this much value in the 22 years we have been in business. Our agents are reporting that TakeWebOrders is the easiest sale they have ever made."</p>
                <p>Stu Rosenbaum<br />CEO, US Merchant Systems</p-->
                <!--/div>
                </div-->
                <!--div class="row text-center col-md-12">
                <div class="col-md-6"><p class="notransition topspace20">
					 <input onClick="parent.location='http://www.takeweborders.com/join.php'" type="submit" id="submit" class="btn btn-large btn-twoblue col-md-5" value="APPLY NOW" />
				</p>
                </div-->
                <!--div class="col-md-6  text-center">
				</div-->
                <!--div class="col-md-3" style="margin: 30px 0;"></div>
                </div-->
				<!--div class="text-center smalltitle">
				</div>
				<div class="col-md-12">
					<div class="list_carousel">
						<div class="carousel_nav">
							<a class="prev" id="car_prev" href="#" style="display: block;"><span>prev</span></a>
							<a class="next" id="car_next" href="#" style="display: block;"><span>next</span></a>
						</div>
						<div class="clearfix">
						</div>
						<div class="caroufredsel_wrapper">
							<ul id="carousel-projects">
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo1.png" class="imgOpa grayimage grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo2.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo3.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo4.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo5.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo6.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo4.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
								<li>
								<div class="featured-projects">
									<div class="featured-projects-image">
										<a href="#"><img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/logos/logo5.png" class="imgOpa grayimage" alt=""></a>
									</div>
								</div>
								</li>
							</ul-->
                            
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>		
		<!-- BEGIN CALL TO ACTION PANEL
================================================== -->
		<!--section class="container animated fadeInDownNow notransition topspace40">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
					<p class="bigtext">
						 Praesent <span class="fontpacifico colortext">WowThemes</span> sapien, a vulputate enim auctor vitae
					</p>
					<p>
						 Duis non lorem porta, adipiscing eros sit amet, tempor sem. Donec nunc arcu, semper a tempus et, consequat
					</p>
				</div>
				<div class="text-center topspace20">
					<a href="#" class="buttonblack"><i class="icon-shopping-cart"></i>&nbsp; get theme</a>
					<a href="#" class="buttoncolor"><i class="icon-link"></i>&nbsp; learn more</a>
				</div>
			</div>
		</div>
		</section>
		<!-- /. end call to action
	</div-->
	<!-- /.wrapsemibox end-->
	<!-- BEGIN FOOTER
================================================== -->
	<section>
	

<?php include 'footer.php'?>