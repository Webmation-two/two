
<?php include 'header.php'?>
	<!-- PAGE TITLE
================================================== -->
	<section class="pageheader-default-contactus pageheader-default text-center">
	<div class="semitransparentbg">
		<h1 class="animated fadeInLeftBig notransition" style="font-weight:100;">Contact Us</h1>
		<p class="animated fadeInRightBig notransition container page-description">&nbsp;
		</p>
	</div>
	</section>
	<div class="wrapsemibox">
		<div class="semiboxshadow text-center">
			<img src="img/shp.png" class="img-responsive" alt="">
		</div>
		<!-- CONTACT
================================================== -->
		<!--iframe class="gmap" style="width:100%;height:370px;border: 0;margin-top:-40px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d198741.04127460168!2d-77.01457599999996!3d38.89359645000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c6de5af6e45b%3A0xc2524522d4885d2a!2sWashington%2C+DC!5e0!3m2!1sen!2sus!4v1395780256271">
		</iframe-->
		<section class="container">
		<div class="row">
			<div class="col-md-8 animated fadeInLeft notransition">
				<h1 class="smalltitle">
				<span>Get in Touch</span>
				</h1>
				<form action="#" name="MYFORM" id="MYFORM">
					<input name="name" size="30" type="text" id="name" class="col-md-6 leftradius" placeholder="Your Name">
					<input name="email" size="30" type="text" id="email" class="col-md-6 rightradius" placeholder="E-mail Address">
					<br />
					<textarea id="message" name="message" class="col-md-12 col-sm-12 allradius" placeholder="Message" rows="9"></textarea>
					<img src="contact/refresh.jpg" width="25" alt="" id="refresh"/><img src="contact/get_captcha.php" alt="" id="captcha"/>
					<br/><input name="code" type="text" id="code" placeholder="Enter Captcha" class="top10">
					<br/>
					<input value="Send" type="submit" id="Send" class="btn btn-default btn-md">
				</form>
			</div>
			<div class="col-md-4 animated fadeInRight notransition">
				<h1 class="smalltitle">
				<span>Locations</span></h1>
				<h4 class="font100">Headquarters</h4>
				<ul class="unstyled">
					<li><span style="margin-right:5px;"><i class="icon-map-marker"></i></span> 2010 Corporate Ridge Ste 700</li>
					<li><span style="margin-right:4px;"><i class="icon-globe"></i></span>McLean, VA 22102</li>
					<li><span style="margin-right:3px;"><i class="icon-phone"></i></span> Phone: 855-WEBMATION</li>
					<!--li><span style="margin-right:2px;"><i class="icon-envelope"></i></span> Email: <a href="mailto:info@takeweborders.com">info@takeweborders.com</a></li-->
				</ul> 
                <p>&nbsp;</p>
			  <h4 class="font100">24/7 Support</h4>
				<ul class="unstyled">
					<li><i class="icon-comment"></i> Chat</li>
					<li><i class="icon-envelope"></i> Email</li>
                    <li><i class="icon-ticket"></i> Ticket Support</li>
					<li><i class="icon-book"></i> Knowledge Base</li>
				</ul>
			</div>
		</div>
		</section>
		<!--CALL TO ACTION PANEL
================================================== -->
		<!--section class="container animated fadeInDownNow notransition topspace40">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
					<p class="bigtext">
						 Praesent <span class="fontpacifico colortext">WowThemes</span> sapien, a vulputate enim auctor vitae
					</p>
					<p>
						 Duis non lorem porta, adipiscing eros sit amet, tempor sem. Donec nunc arcu, semper a tempus et, consequat
					</p>
				</div>
				<div class="text-center topspace20">
					<a href="#" class="buttonblack"><i class="icon-shopping-cart"></i>&nbsp; get theme</a>
					<a href="#" class="buttoncolor"><i class="icon-link"></i>&nbsp; learn more</a>
				</div>
			</div>
		</div>
		</section-->
		<!-- /. end call to action-->
        <br /><br />
	</div>
	<!-- /.wrapsemibox end-->
	<!-- BEGIN FOOTER
================================================== -->
	
<?php include 'footer.php'?>