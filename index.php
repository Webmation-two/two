
<?php include 'header.php'; ?>
	<!--section class="pageheader-default"> 
		<!--div class="container">
        <div class="semitransparentbg">
			<!--div class="row"> 
				
                <!--div class="col-md-12 row" >
                
					<form> 
						<h1 class="landingpage animated fadeInLeftBig notransition topspace20">Take Web Orders</h1>
						<h3 style="font-weight:300;">Restaurants Accept Orders Everywhere</h3>
					  <h3 class="landingpage animated fadeInDownBig notransition"> </h3>
                        <span class="col-md-2"> </span>
						<input class="form-field text-input email col-md-3" style="margin-right:3%;" name="email" placeholder="Email Address" required="required" type="email">
						<input class="form-field text-input password col-md-3" style="margin-right:3%;" name="password" placeholder="Password" required="required" type="password">
						<input type="submit" id="submit" class="btn btn-invert btn-twoblue col-md-2" value="Get Started">							 
					</form>
                    
                    <span class="col-md-2"> </span>
				<!--div class="col-md-12">
				<img src="img/computers.png" width="100%">
				</div>
				</div>
				</div>
			</div> 
			
		</div>
        </section-->
	<section class="pageheader-default text-center" style="position: relative">
    <div class="semitransparentbg">
		<h1  style="font-weight: 100;">Take Web Orders</h1>
		<!--p class="animated fadeInRightBig notransition container page-description">
			 Bringing online ordering from anywhere<br />
             to restaurants and their owners worldwide.
		</p-->
        <div class="row col-md-12" style="padding-top:25px;">
        <form>
        <div class="col-md-4 text-center"></div>
        <div class="col-md-4 text-center">
        <!--input class="form-field text-input email" style="margin-right:3%;" name="email" placeholder="Email Address" required="required" type="email">
        </div>
        <div class="col-md-2 text-center">
		<input class="form-field text-input password" style="margin-right:3%;" name="password" placeholder="Password" required="required" type="password">
        </div>
        <div class="col-md-2 text-center"-->
		<input onClick="parent.location='http://www.takeweborders.com/join.php'" type="submit" id="submit" class="btn btn-invert btn-twoblue" value="Get Started">	
        </div>	
        <div class="col-md-4 text-center"></div>					 
		</form> 
		<div class="col-md-12 col-sm-12 text-center" style="text-align:center;" >
			<img src="img/computers.png" style="height: auto; max-width: 100%;"> 
		</div>
        </div> 
		<div class="row col-md-12  col-sm-12 blackbk blackbkFeature container">
			<div class="container">	
				<div class="col-md-4 col-sm-4">
					<h4 class="whiteText text-center" style="font-weight:300;">4.99% PER TRANSACTION</h4> 
				</div>
				<div class="col-md-4 col-sm-4 text-center" style="padding-top:5px;">
					<img src="img/cards.png" /> 
				</div>
				<div class="col-md-4 col-sm-4" >
					<h4 class="whiteText text-center" style="font-weight:300;">WEEKLY DEPOSITS</h4>
				</div> 
				</div>
			</div>
	</div>
    </section>  
	<div class="wrapsemibox">
			<div class="semiboxshadow text-center">
			<img src="img/shp.png" class="img-responsive" alt="">
	</div> 
	<!--section class="home-features blackbk-default" style="padding-top: 10px; color:#ffffff;" >
		<div class="container">
			<div class="row center-block">
				<div class="col-md-4 text-center">
					<h4 class="whiteText">4.99% PER TRANSACTION</h4> 
				</div>
				<div class="col-md-4 text-center" style="padding-top:5px;">
					<img src="img/cards.png" />
				</div>
				<div class="col-md-4 text-center">
					<h4 class="whiteText">WEEKLY DEPOSITS</h4>
				</div>
			</div>
        </div>		
	</sectio--n> 
	
                    <!--div class="br-hr type_short">
						<span class="br-hr-h">
						<i class="icon-pencil">Test</i>
						</span>
					</div-->
	
		<!--/div-->	
		
	<!-- FEATURES
================================================== -->
		<section class="grayarea home-features">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="margin-top: -10px;">
					<h1 class="small text-center notransition" style="text-transform:uppercase">Accept Delivery, Carry-Out & Catering</h1>
                    <br />
					<!--div class="br-hr type_short">
						<span class="br-hr-h animated fadeInRightNow">
						<i class="icon-pencil"></i>
						</span>
					</div--> 
                    <div class="row notransition">
						<div class="col-md-3 col-sm-6">
							<h4><i class="icon icon-desktop">
							</i> Online</h4>
							<div class="bottomspace30">
							70% of customers order food online. Have a gorgeous website that boosts profits, improves customer service and cuts your costs. <!--a href="#">Learn More</a-->
							</div>
						</div> 
						<div class="col-md-3 col-sm-6">
							<h4><i class="icon icon-mobile-phone">
							</i> On Mobile</h4>
							<div class="bottomspace30">
								 Mobile ordering is skyrocketing. Allow customers to order online on the go with a site designed specifically for smart phones and tablets. <!--a href="#">Learn More</a-->
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<h4><i class="icon icon-facebook">
							</i> On Facebook</h4>
							 Tap into the power of social media and viral marketing by letting customers interact and order directly from your Facebook Fan Page.<br /><br /><!--a href="#">Learn More</a> <br-->
						</div>
                        <div class="col-md-3 col-sm-6"> 
							<h4><i class="icon icon-group">
							</i> In-Person</h4>
							 Fully integrated POS system to take orders at the restaurant, reservations, wait lists, staff management, customer engagement and more. <!--a href="#">Learn More</a> <br-->
						</div>
					</div>
					<!--div class="row animated fadeInUpNow notransition">
						<div class="col-md-4">
							<h4><i class="icon icon-user">
							</i> Ready to Use</h4>
							 Trigger ideas: quickly experiment with site colors &amp; patterns, try out web typography and much more. <br>
						</div>
						<div class="col-md-4">
							<h4><i class="icon icon-cogs">
							</i> Great for clients</h4>
							 Trigger ideas: quickly experiment with site colors &amp; patterns, try out web typography and much more. <br>
						</div>
						<div class="col-md-4">
							<h4><i class="icon icon-leaf">
							</i> MultiPurpose Use</h4>
							<div class="bottomspace30">
								 Trigger ideas: quickly experiment with site colors &amp; patterns, try out web typography and much more.
							</div>
						</div>
					</div-->
				</div>
			</div>
		</div>
		</section>
	<!-- TESTIMONIALS
================================================== -->
		<section class="topspace10">
		<div class="container notransition"> 
			<div class="row">
				<h1 class="text-center smalltitle">
				<span>Testimonials</span>
				</h1>
				<div id="cbp-qtrotator" class="cbp-qtrotator">
					<div class="cbp-qtcontent" style="padding-left:20px;">
						<img src="img/milanas-avatar.png" alt="">
						<blockquote>
							<p class="bigquote">
								<i class="icon-quote-left colortext quoteicon"></i><span style="text-align:left;"> Our TWO site has brought us over $65,000 in online orders and continues to increase sales each month. With the increased revenue, we were able to move to another location!</span>
							</p>
							<footer>Adriel Fasci, Owner of Milana's New York Pizzeria</footer>
						</blockquote>
					</div>
					<!--div class="cbp-qtcontent">
						<img src="http://wowthemes.net/demo/biscaya/img/demo/avatar.jpg" alt="">
						<blockquote>
							<p class="bigquote">
								<i class="icon-quote-left colortext quoteicon"></i> Lorem ipsum dolor sit adipiscing elit. Praesent tempus eleifend risus ut congue eset nec lacus. Lorem ipsum dolor sit adipiscing elit. Praesent tempus eleifend risus ut congue eset nec lacus. Praesent dignissim sem sapien, a vulputate enim auctor vitae. Duis non lorem porta, adipiscing eros sit amet, tempor sem.
							</p>
							<footer>Pino Caruso / Director of <a href="#">example.com</a></footer>
						</blockquote>
					</div-->
				</div>
			</div>
		</div>
		</section>		
		<!-- BEGIN CALL TO ACTION PANEL
================================================== -->
		<section class="container notransition topspace40">
		<div class="row">
			<div class="col-md-12">
				<!--div class="text-center">
					<p class="bigtext">
						 Praesent <span class="fontpacifico colortext">WowThemes</span> sapien, a vulputate enim auctor vitae
					</p>
					<p>
						 Duis non lorem porta, adipiscing eros sit amet, tempor sem. Donec nunc arcu, semper a tempus et, consequat
					</p>
				</div-->
				<!--div class="text-center topspace20">
					<a href="#" class="buttoncolor btn-large btn-twoblue"><i class="icon-shopping-cart"></i>&nbsp; get your free site</a>
					<!--a href="#" class="buttoncolor"><i class="icon-link"></i>&nbsp; learn more</a-->
				</div-->
			</div>
		</div>
		</section>
		<!-- /. end call to action-->
	</div>
	<!-- /.wrapsemibox end-->
	
<?php include 'free-with-two.php'?>
<?php include 'footer.php'?>