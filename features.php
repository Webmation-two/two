<?php include 'header.php'?>

<!-- PAGE TITLE
================================================== -->
	<section class="pageheader-default-features pageheader-default text-center pageheader-default-responsive" style="position: relative;">
    <div class="semitransparentbg">
		<h1 class="animated fadeInLeftBig notransition" style="font-weight: 100;">Accept Orders Everywhere</h1>
		<p class="animated fadeInRightBig notransition container page-description">
			 TakeWebOrders is packed with<br />
             features proven to increase your profits.
		</p>
	</div>
	<!--section class="home-features blackbkFeature blackbk"-->
		<!--div class="container"-->
			<!--div class="row center-block">
				<div class="col-md-4 text-center">
					<h4 class="whiteText">4.99% PER TRANSACTION</h4> 
				</div>
				<div class="col-md-4 text-center" style="padding-top:5px;">
					<img src="img/cards.png" />
				</div> 
				<div class="col-md-4 text-center"> 
					<h4 class="whiteText">WEEKLY DEPOSITS</h4>
				</div-->
			<!--/div-->		
	<!--/section--> 
    
	
	</section>
	<div class="wrapsemibox">
		<div class="semiboxshadow text-center"> 
			<img src="img/shp.png" class="img-responsive" alt="">
	</div>
	
		<!-- SERVICES
================================================== -->
	<section class="home-features">
		<div class="container">
				<h1 class="small text-center" style="color:#333;">FEATURES & PRICING</h1>
        </div>

	
                    <!--div class="br-hr type_short">
						<span class="br-hr-h">
						<i class="icon-pencil">Test</i>
						</span>
					</div-->
	
		</div>
		</section>	
		<!-- COLUMNS
================================================== -->

	<!-- FAQs
================================================== -->
		<section class="container notransition">
        <div class="container">
			<div class="row center-block">
				<div class="col-md-4 col-sm-4 text-center">
					<h4 style="font-weight:300;">4.99% PER TRANSACTION</h4>
                    <img src="img/icons/two-cart.png"><br /><br />
                    <div class="text-left">
                    <p>TakeWebOrders includes everything you need to save thousands a year on third party services and includes all Visa, MasterCard, Discover, and American Express fees!<br /></p></div>
				</div>
				<div class="col-md-4 col-sm-4 text-center"> 
					<h4 style="font-weight:300;">WEEKLY DEPOSITS</h4>
                    <img src="img/icons/two-deposits.png"><br /><br />
                    <div class="text-left">
                    <p>Don't wait 30 days to get your funds deposited! Managing your cash flow is critical to your restaurant's success. Get your money weekly with TakeWebOrders.<br /></p></div>
				</div>
                <div class="col-md-4 col-sm-4 text-center">
					<h4 style="font-weight:300;">NOTIFICATION OPTIONS</h4>
                	<img src="img/icons/two-notifications.png"><br /><br />
                	<div class="text-left">
						<ul><li class="none">Email & IP Printer: Free</li>
                			<li class="none">Fax with IVR: 20&cent;/order</li>
                			<li class="none">SMS Printer: $299.95 + $25/month</li>
                			<li class="none">POS (Coming Soon): Free</li>
                		</ul> 
                    </div>
				</div> <br /><br /><br />
                <div class="row center-block col-md-12 text-center">
                <div class="text-center" style="margin-top:10px;">
                <iframe src="//player.vimeo.com/video/74399068" width="650" height="366" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <!--iframe width="640" height="360" src="//www.youtube.com/embed/LSvwf7YHMGg?rel=0" frameborder="0" allowfullscreen></iframe-->
                <!--iframe width="853" height="480" src="//www.youtube.com/embed/LSvwf7YHMGg?rel=0" frameborder="0" allowfullscreen></iframe-->
                </div>
                <p style="font-style:italic; padding-top:15px;">Plans with free cash orders, lower discount rates, daily deposits, and free order notifications are also available.</p></div>
                <div class="row center-block col-md-12 text-center">
                <h3 style="font-weight:300; text-transform:capitalize; padding-bottom:5px;">No commitments. no minimums. Cancel anytime.</h3>
                </div></div>
			</div>		
            <br />
		<!-- 1ST ROW
	================================================== -->
		<!--div class="row">
			<!-- BLOCK 1 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tags faqsicon"></span> Website</h1>
				<dl class="faqs">
                <dt></dt>
					<dd>
                    <ul>
                    <li>Admin Portal and Dashboard</li>
					<li>Restaurant Theme Designer</li>
					<li>Content Management System</li>
					<li>Email Notifi cation Editor</li>
					<li>Real-Time Order Management</li>
					<li>Real-Time Customer Management</li>
					<li>Real-Time Menu Management</li>
					<li>Real-Time Coupon Management</li>
					<li>Real-Time Reports</li>
					<li>Employee Departments</li>
					<li>Restricted Employee Accounts</li>
                    </ul>
					</dd>
				</dl>
			</div>
            
			<!-- end block 1
			<!-- BLOCK 2 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-flag faqsicon"></span> Advanced Order Features</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Accept Carry Out / Take Out Orders</li>
                    <li>Accept Delivery Orders</li>
                    <li>Accept Cash Orders</li>
                    <li>Accept Credit Cards</li>
                    <li>Sales Tax</li>
                    <li>Delivery Fees</li>
                    <li>Easy Checkout</li>
                    <li>One Page Menu</li>
                    <li>Menu Memory</li>
                    <li>Anonymous Guest Checkout</li>
                    <li>Require Customer Registration</li>
                    <li>Account Registration Incentives</li>
                    <li>Future Ordering</li>
                    <li>Holiday & Closed Dates</li>
                    <li>Require Customer Signature</li>
                    <li>Order Minimums</li>
                    <li>Order History</li>
                    <li>Customer Quick Reorder</li>
                    <li>Customer Favorite Foods List</li>
                    <li>Tip Calculator & Reminder</li>
                    <li>Menu Item Images</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- end block 2
		<!-- 2ND ROW
	================================================== 
			<!-- BLOCK 3 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tasks faqsicon"></span> Advanced Menu Features</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Food Categories & Menu Items</li>
                    <li>Item Attributes & Options</li>
                    <li>Toppings (1/2, Normal, Double)</li>
                    <li>Combo Meals & Make It A Combo</li>
                    <li>Special Offers</li>
                    <li>Featured and Popular Items</li>
                    <li>Upsells On Checkout</li>
                    <li>Lunch & Dinner Specials</li>
                    </ul>
                    </dd>
				</dl>
			</div>
            </div>
            
			<!-- end block 3
            <div class="row">
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Advanced Delivery Management</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Set Delivery Radius</li>
                    <li>Customer Address Book</li>
                    <li>Web Order Review</li>
                    <li>Email Notifi cations</li>
                    <li>SMS Printer Ready</li>
                    <li>Fax and IVR Notifi cation Ready</li>
                    <li>Multiple Location Ready</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- end block 4 
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Catering Menu Features</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Catering Categories</li>
                    <li>Catering Menu Items</li>
                    <li>Catering Advanced Notice</li>
                    <li>Catering Toppings Price</li>
                    <li>Catering Delivery Options</li>
                    </ul>
                    </dd>
				</dl>
			</div>
	
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Coupon Management</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Free Item Coupons</li>
                    <li>Dollar Off Coupons</li>
                    <li>Percent Off Coupons</li>
                    <li>Limited Time Coupons</li>
                    <li>Per Customer Coupons</li>
                    <li>Limited Number Coupons</li>
                    <li>First Order Discount Coupons</li>
                    <li>Combination Coupons</li>
                    <li>Public & Private Coupons</li>
                    <li>Restrict Coupons (Carry Out, Dine In)</li>
                    <li>Facebook-Only Coupons</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- end block 4 
		</div>
                    <div class="row">
			<!-- BLOCK 4
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> HTML Email Marketing</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Website Integration</li>
                    <li>Auto Customer List Updates</li>
                    <li>Newsletter Lists</li>
                    <li>Custom Email Lists</li>
                    <li>Autoresponders / Drip Campaigns</li>
                    <li>Real-Time Email Statistic Tracking</li>
                    <li>125+ Email Templates</li>
                    <li>Create Your Own Email Designs</li>
                    <li>A/B Split Testing Capabilities</li>
                    <li>Customer Birthday Emails</li>
                    <li>SMS Text Messaging Ready</li>
                    <li>Spam-Act Compliance</li>
                    <li>Highest Deliverability Rates</li>
                    <li>Import/Export Functionality</li>
                    <li>10MB Media Library Included</li>
                    <li>500 Email Contacts</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Social Media Integration</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Facebook</li>
                    <li>Google+</li>
                    <li>Twitter</li>
                    <li>Social Media Best Practices Guide</li>
                    <li>Facebook Fan Page Ordering</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Loyalty & Referral Program Management</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Real-Time Rewards Management</li>
                    <li>Redeem Points for Menu Items</li>
                    <li>Customer Rewards Center</li>
                    <li>Registration Bonus Points</li>
                    <li>Referral Bonus Points</li>
                    <li>Referral Sign Up Points</li>
                    <li>Points for Social Media Order Posts</li>
                    <li>Rewards Point Totals on Receipts</li>
                    <li>Set Item & Topping Point Values</li>
                    <li>Set Point(s) Earned Per Dollar(s) Spent</li>
                    <li>Manual Email Input</li>
                    <li>Email Referral Automation</li>
                    <li>Social Media Referral Automation</li>
                    <li>Referral Tracking and Status</li>
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- end block 4 
		</div>
                    <div class="row">
			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Gift Card Program Management</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Sell Online Gift Certificates / Cards</li>
                    <li>Pre-Defined Gift Card Templates</li>
                    <li>Upload Custom Gift Card Designs</li>
                    <li>Customize Gift Card Emails</li>
                    <li>Check Gift Card Balances</li>
                    <li>Split Payments</li>
                    </ul>
                    </dd>
				</dl>
			</div>

			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Advanced Technology & Security</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>No Software Needed</li>
                    <li>True Mobile Site</li>
                    <li>Vanity TWO URL</li>
                    <li>Use Your Own Domain</li>
                    <li>Domain Email Addresses</li>
                    <li>Daily Off Site Data Back Ups</li>
                    <li>Secure Merchant Processing</li>
                    <li>Industry Standard SSL</li>
                    <li>Database Encryption</li>
                    <li>Cloud Based Hosting</li>
                    <li>Daily Virus and Intrusion Scans</li>
                    <li>Server Patches & Maintenance</li>
                    <li>Automatic Upgrades</li>
                    <li>Google Maps Integration</li>
                    <li>AT&T Wireless Network</li>
                    <liSEO Functionality</li>
                    
                    </ul>
                    </dd>
				</dl>
			</div>
			<!-- end block 4 

			<!-- BLOCK 4 
			<div class="col-md-4">
				<h1 class="faqstitle"><span class="icon-tint faqsicon"></span> Technical & Legal Support</h1>
				<dl class="faqs">
					<dt></dt>
					<dd>
                    <ul>
                    <li>Webmation Helpdesk</li>
                    <li>Online Knowledgebase</li>
                    <li>OnDemand Video Tutorials</li>
                    <li>Standard Case Support</li>
                    <li>Telephone Support</li>
            
                    </ul>
                    </dd>
				</dl> 
			</div>
			<!-- end block 
		</div>
        </div--></section>
		<section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						EASY MANAGEMENT
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Admin Portal and Dashboard</td>
                    </tr>
                    <tr>
					<td>Restaurant Theme Designer</td></tr>
                    <tr>
					<td>Content Management System</td></tr>
                    <tr>
					<td>Email Notification Editor</td></tr>
                    <tr>
					<td>Real-Time Order Management</td></tr>
                    <tr>
					<td>Real-Time Customer Management</td></tr>
                    <tr>
					<td>Real-Time Menu Management</td></tr>
                    <tr>
					<td>Real-Time Coupon Management</td></tr>
                    <tr>
					<td>Real-Time Reports</td></tr>
                    <tr>
					<td>Employee Departments</td></tr>
                    <tr>
					<td>Restricted Employee Accounts</td></tr>
				<!--tr>
					<td>
						 2
					</td>
					<td>
						 Mark
					</td>
					<td>
						 Otto
					</td>
					<td>
						 @TwBootstrap 
					</td>
				</tr>
				<tr>
					<td>
						 2
					</td>
					<td>
						 Jacob
					</td>
					<td>
						 Thornton
					</td>
					<td>
						 @fat
					</td>
				</tr>
				<tr>
					<td>
						 3
					</td>
					<td colspan="2">
						 Larry the Bird
					</td>
					<td>
						 @twitter
					</td>
				</tr-->
				</tbody>
				</table>
		</section>
		<br /><br />
        
        <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						ADVANCED ORDER FEATURES
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>
                    Accept Carry Out / Take Out Orders</td>
                    </tr>
                    <tr>
                    <td>Accept Delivery Orders</td>
                    </tr>
                    <!--tr>
                    <td>Accept Cash Orders</td>
                    </tr-->
                    <tr>
                    <td>Accept Credit Cards</td>
                    </tr>
                    <tr>
                    <td>Sales Tax</td>
                    </tr>
                    <tr>
                    <td>Delivery Fees</td>
                    </tr>
                    <tr>
                    <td>Easy Checkout</td>
                    </tr>
                    <tr>
                    <td>One Page Menu</td>
                    </tr>
                    <tr>
                    <td>Menu Memory</td>
                    </tr>
                    <tr>
                    <td>Anonymous Guest Checkout</td>
                    </tr>
                    <tr>
                    <td>Require Customer Registration</td>
                    </tr>
                    <tr>
                    <td>Account Registration Incentives</td>
                    </tr>
                    <tr>
                    <td>Future Ordering</td>
                    </tr>
                    <tr>
                    <td>Holiday & Closed Dates</td>
                    </tr>
                    <tr>
                    <td>Require Customer Signature</td>
                    </tr>
                    <tr>
                    <td>Order Minimums</td>
                    </tr>
                    <tr>
                    <td>Order History</td>
                    </tr>
                    <tr>
                    <td>Customer Quick Reorder</td>
                    </tr>
                    <tr>
                    <td>Customer Favorite Foods List</td>
                    </tr>
                    <tr>
                    <td>Tip Calculator & Reminder</td>
                    </tr>
                    <tr>
                    <td>Menu Item Images</td>
                    </tr>
                    
				</tbody>
				</table>
		</section>
		<br /><br />
        
           <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						ADVANCED MENU FEATURES
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Food Categories & Menu Items</td>
                    </tr>
                    <tr>
                    <td>Item Attributes & Options</td>
                    </tr>
                    <tr>
                    <td>Toppings (1/2, Normal, Double)</td>
                    </tr>
                    <tr>
                    <td>Combo Meals & Make It A Combo</td>
                    </tr>
                    <tr>
                    <td>Special Offers</td>
                    </tr>
                    <tr>
                    <td>Featured and Popular Items</td>
                    </tr>
                    <tr>
                    <td>Upsells On Checkout</td>
                    </tr>
                    <tr>
                    <td>Lunch & Dinner Specials</td>
                    </tr>
                    
				</tbody>
				</table>
		</section>
		<br /><br />
        
           <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						DELIVERY MANAGEMENT
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Set Delivery Radius</td>
                    </tr>
                    <tr>
                    <td>Customer Address Book</td>
                    </tr>
                    <tr>
                    <td>Web Order Review</td>
                    </tr>
                    <tr>
                    <td>Email Notifications</td>
                    </tr>
                    <tr>
                    <td>SMS Printer Ready</td>
                    </tr>
                    <tr>
                    <td>Fax and IVR Notification Ready</td>
                    </tr>
                    <tr>
                    <td>IP Printer Ready</td>
                    </tr>
                    <tr>
                    <td>Multiple Location Ready</td></tr>
				</tbody>
				</table>
		</section>
		<br /><br />
        
           <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						CATERING MENU FEATURES
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Catering Categories</td>
                    </tr>
                    <tr>
                    <td>Catering Menu Items</td>
                    </tr>
                    <tr>
                    <td>Catering Advanced Notice</td>
                    </tr>
                    <tr>
                    <td>Catering Toppings Price</td>
                    </tr>
                    <tr>
                    <td>Catering Delivery Options</td></tr>
				</tbody>
				</table>
		</section>
		<br /><br />
        
           <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						COUPON MANAGEMENT
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Free Item Coupons</td>
                    </tr>
                    <tr>
                    <td>Dollar Off Coupons</td>
                    </tr>
                    <tr>
                    <td>Percent Off Coupons</td>
                    </tr>
                    <tr>
                    <td>Limited Time Coupons</td>
                    </tr>
                    <tr>
                    <td>Per Customer Coupons</td>
                    </tr>
                    <tr>
                    <td>Limited Number Coupons</td>
                    </tr>
                    <tr>
                    <td>First Order Discount Coupons</td>
                    </tr>
                    <tr>
                    <td>Combination Coupons</td>
                    </tr>
                    <tr>
                    <td>Public & Private Coupons</td>
                    </tr>
                    <tr>
                    <td>Restrict Coupons (Carry Out, Dine In)</td>
                    </tr>
                    <tr>
                    <td>Facebook-Only Coupons</td></tr>
				</tbody>
				</table>
		</section>
       
		<br /><br />
        
               <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						HTML EMAIL MARKETING
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Website Integration</td>
                    </tr>
                    <tr>
                    <td>Auto Customer List Updates</td>
                    </tr>
                    <tr>
                    <td>Newsletter Lists</td>
                    </tr>
                    <tr>
                    <td>Custom Email Lists</td>
                    </tr>
                    <tr>
                    <td>Autoresponders / Drip Campaigns</td>
                    </tr>
                    <tr>
                    <td>Real-Time Email Statistic Tracking</td>
                    </tr>
                    <tr>
                    <td>125+ Email Templates</td>
                    </tr>
                    <tr>
                    <td>Create Your Own Email Designs</td>
                    </tr>
                    <tr>
                    <td>A/B Split Testing Capabilities</td>
                    </tr>
                    <tr>
                    <td>Customer Birthday Emails</td>
                    </tr>
                    <tr>
                    <td>SMS Text Messaging Ready</td>
                    </tr>
                    <tr>
                    <td>Spam-Act Compliance</td>
                    </tr>
                    <tr>
                    <td>Highest Deliverability Rates</td>
                    </tr>
                    <tr>
                    <td>Import/Export Functionality</td>
                    </tr>
                    <tr>
                    <td>10MB Media Library Included</td>
                    </tr>
                    <tr>
                    <td>500 Email Contacts Included</td></tr>
				</tbody>
				</table>
		</section>
       
		<br /><br />
        
               <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						SOCIAL MEDIA INTEGRATION
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td> Facebook</td>
                    </tr>
                    <tr>
                    <td>Google+</td>
                    </tr>
                    <tr>
                    <td>Twitter</td>
                    </tr>
                    <tr>
                    <td>Social Media Best Practices Guide</td>
                    </tr>
                    <tr>
                    <td>Facebook Fan Page Ordering</td></tr>
				</tbody>
				</table>
		</section>
       
		<br /><br />
        
               <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						LOYALTY & REFERRAL PROGRAM
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Real-Time Rewards Management</td>
                    </tr>
                    <tr>
                    <td>Redeem Points for Menu Items</td>
                    </tr>
                    <tr>
                    <td>Customer Rewards Center</td>
                    </tr>
                    <tr>
                    <td>Registration Bonus Points</td>
                    </tr>
                    <tr>
                    <td>Referral Bonus Points</td>
                    </tr>
                    <tr>
                    <td>Referral Sign Up Points</td>
                    </tr>
                    <tr>
                    <td>Points for Social Media Order Posts</td>
                    </tr>
                    <tr>
                    <td>Rewards Point Totals on Receipts</td>
                    </tr>
                    <tr>
                    <td>Set Item & Topping Point Values</td>
                    </tr>
                    <tr>
                    <td>Set Point(s) Earned Per Dollar(s) Spent</td>
                    </tr>
                    <tr>
                    <td>Manual Email Input</td>
                    </tr>
                    <tr>
                    <td>Email Referral Automation</td>
                    </tr>
                    <tr>
                    <td>Social Media Referral Automation</td>
                    </tr>
                    <tr>
                    <td>Referral Tracking and Status</td></tr>
				</tbody>
				</table>
		</section>
       
		<br /><br />
        
               <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						GIFTCARD PROGRAM
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Sell Online Gift Certificates / Cards</td>
                    </tr>
                    <tr>
                    <td>Pre-Defined Gift Card Templates</td>
                    </tr>
                    <tr>
                    <td>Upload Custom Gift Card Designs</td>
                    </tr>
                    <tr>
                    <td>Customize Gift Card Emails</td>
                    </tr>
                    <tr>
                    <td>Check Gift Card Balances</td>
                    </tr>
                    <tr>
                    <td>Split Payments</td></tr>
				</tbody>
				</table>
		</section>
       
		<br /><br />
        
                       <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						ADVANCED TECHNOLOGY & SECURITY
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>No Software Needed</td>
                    </tr>
                    <tr>
                    <td>True Mobile Site</td>
                    </tr>
                    <tr>
                    <td>Vanity TWO URL</td>
                    </tr>
                    <tr>
                    <td>Use Your Own Domain</td>
                    </tr>
                    <tr>
                    <td>Domain Email Addresses</td>
                    </tr>
                    <tr>
                    <td>Daily Off Site Data Back Ups</td>
                    </tr>
                    <tr>
                    <td>Secure Merchant Processing</td>
                    </tr>
                    <tr>
                    <td>Industry Standard SSL</td>
                    </tr>
                    <tr>
                    <td>Database Encryption</td>
                    </tr>
                    <tr>
                    <td>Cloud Based Hosting</td>
                    </tr>
                    <tr>
                    <td>Daily Virus and Intrusion Scans</td>
                    </tr>
                    <tr>
                    <td>Server Patches & Maintenance</td>
                    </tr>
                    <tr>
                    <td>Automatic Upgrades</td>
                    </tr>
                    <tr>
                    <td>Google Maps Integration</td>
                    </tr>
                    <tr>
                    <td>AT&amp;T Wireless Network</td>
                    </tr>
                    <tr>
                    <td>SEO Functionality</td></tr>
				</tbody>
				</table>
		</section><br /><br />
        
                       <section  class="container">
		<table class="table table-striped" style="width:80%;margin:0 auto; border: 1px #e2e2e2 solid;">
				<thead>
				<tr STYLE="background: #e7402f;">
					<th>
						TECHNICAL SUPPORT
					</th>
					<!--th>
						 IPAD
					</th>
					<th>
						 IPHONE
					</th> 
					<th>
						WEB
					</th-->
				</tr>
				</thead>
				<tbody>
					<tr>
					<td>Webmation Helpdesk</td>
                    </tr>
                    <tr>
                    <td>Online Knowledgebase</td>
                    </tr>
                    <tr>
                    <td>OnDemand Video Tutorials</td>
                    </tr>
                    <tr>
                    <td>Standard Case Support</td>
                    </tr>
                    <tr>
                    <td>Telephone Support</td></tr>
				</tbody>
				</table>
		</section>
        <br /><br />
                      
              

			<!--One Half + One Half
    ================================================== 
			<div class="row">
				<div class="col-md-6">
					<h4>One Half</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Donec fringilla libero condimentum ligula tempus dapibus. Nulla interdum ante quis nisi dignissim sed tempus libero fermentum. In hac habitasse platea dictumst. Sed non arcu luctus quam accumsan iaculis. Integer molestie, massa eu lacinia rhoncus, elit ligula egestas mi, et aliquam turpis neque at dolor.
				</div>
				<div class="col-md-6">
					<h4>One Half</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Donec fringilla libero condimentum ligula tempus dapibus. Nulla interdum ante quis nisi dignissim sed tempus libero fermentum. In hac habitasse platea dictumst. Sed non arcu luctus quam accumsan iaculis. Integer molestie, massa eu lacinia rhoncus, elit ligula egestas mi, et aliquam turpis neque at dolor.
				</div>
			</div>
			<br>
			<!--One Third + One Third + One Third
    ================================================== 
			<div class="row">
				<div class="col-md-4">
					<h4>One Third</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Donec fringilla libero condimentum ligula tempus dapibus.
				</div>
				<div class="col-md-4">
					<h4>One Third</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Donec fringilla libero condimentum ligula tempus dapibus.
				</div>
				<div class="col-md-4">
					<h4>One Third</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Donec fringilla libero condimentum ligula tempus dapibus.
				</div>
			</div>
			<br>
			<!--One Fourth + One Fourth + One Fourth + One Fourth 
    ================================================== 
			<div class="row">
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
			</div>
			<br>
			<!--One Half + One Fourth + One Fourth 
    ================================================== 
			<div class="row">
				<div class="col-md-6">
					<h4>One Half</h4>
					 Dolor hendrerit melior quae roto sagaciter vel. Abigo dolor importunus tation velit vulputate. Acsi adipiscing immitto lenis scisco. Ad causa distineo incassum lobortis macto nimis probo ratis sed. Abico causa conventio interdico iusto melior obruo quis vicis virtus. Et qui suscipit. Bene eu hos scisco suscipere tation. Abico autem consectetuer decet euismod fere luptatum molior scisco sed.
				</div>
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Consectetuer paratus tincidunt ullamcorper venio vero. Diam dolus iusto jus populus quibus quidem volutpat wisi. Consequat paulatim refero tation tincidunt. Acsi dolus interdico jus.
				</div>
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Consectetuer paratus tincidunt ullamcorper venio vero. Diam dolus iusto jus populus quibus quidem volutpat wisi. Consequat paulatim refero tation tincidunt. Acsi dolus interdico jus.
				</div>
			</div>
			<br>
			<!--One Fourth + Three Fourth
    ================================================== 
			<div class="row">
				<div class="col-md-3">
					<h4>One Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
				<div class="col-md-9">
					<h4>Three Fourth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam tincidunt, felis vel varius tristique, dui nisi mollis massa, vel porta elit libero ullamcorper lacus.
				</div>
			</div>
			<br>
			<!--One Sixth + One Sixth + One Sixth + One Sixth + One Sixth + One Sixth + 
    =============================================================================== 
			<div class="row">
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
				<div class="col-md-2">
					<h4>One Sixth</h4>
					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
				</div>
			</div>
			<br>
			</section>
		</div>
		<!--CALL TO ACTION PANEL
================================================== -->
	
<?php include 'free-with-two.php'?>
<?php include 'footer.php'?>