<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content=""> 
<meta name="author" content="">
<title>TakeWebOrders - Restaurant Online Ordering Websites</title>
<!-- Style -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<!-- Choose Layout -->
<link href="css/layout-wide.css" rel="stylesheet">
<!-- Choose Skin -->
<link href="css/skin-blue.css" rel="stylesheet">
<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon.ico">
<!-- IE -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>	   
<![endif]-->
<!--[if lte IE 8]>
<link href="css/ie8.css" rel="stylesheet">
<![endif]-->
</head>
<body class="off">
<!-- /.wrapbox start-->
<div class="wrapbox">
	<!-- TOP AREA
================================================== -->
	<!--section class="toparea">
	<div class="container">
		<div class="row"> 
			<div class="col-md-6 top-text pull-left">
				<i class="icon-phone"></i> Phone: (316) 444 8529 <span class="separator"></span><i class="icon-envelope"></i> Email: <a href="#">wowthemesnet@gmail.com</a>
			</div>
			<div class="col-md-6 text-right">
				<div class="social-icons">
					<a class="icon icon-facebook" href="http://www.facebook.com/takeweborders"></a>
					<a class="icon icon-twitter" href="http://twitter.com/takeweborders"></a>
					<a class="icon icon-google-plus" href="#"></a>
					
				</div>
			</div>  
		</div>
	</div>
	</section-->
	<!-- /.toparea end-->
	<!-- NAV
<!-- NAV
================================================== -->
	<?php
	$pageName = basename($_SERVER['PHP_SELF']);
	?>
	<nav class="navbar navbar-fixed-top wowmenu" role="navigation">
	<div class="container"> 
		<div>
			<a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="logo"></a>
		</div>
		<ul id="nav" class="nav navbar-nav pull-right"> 
			<li <? if($pageName == "index.php" || $pageName == ""){ echo "class=active"; } ?>><a href="/clients/twotest/">Home</a></li>
			<li <? if($pageName == "features.php"){ echo "class=active"; } ?>><a href="features.php">Features</a></li>
			<li <? if($pageName == "about.php"){ echo "class=active"; } ?>><a href="about.php">About</a></li> 
            <li <? if($pageName == "clients/twotest/blog"){ echo "class=active"; } ?>><a href="/blog">Blog</a></li> 
			<li <? if($pageName == "contact-us.php"){ echo "class=active"; } ?>><a href="contact-us.php">Contact</a></li>	
            <li style="margin-top: 10px;"><input onClick="parent.location='https://admin.takeweborders.com/'" type="submit" id="submit" class="btn btn-large btn-twoblue" value="LOGIN" /></li>				
		</ul>
         
	</div>
	</nav>
	<!-- /nav end-->
    