<section>
	<div class="footer">
		<div class="container animated fadeInUpNow notransition">
			<div class="row">
				<div class="col-md-3">
                	<img src="img/two-site-logo-grayscale.png" />
                    <br />
					<!--h1 class="footerbrand">TakeWebOrders</h1-->
					<p>
						 Online ordering and websites for restaurants.
					</p>
					<p>
						 POS System Coming Soon. <br />Increase profits. Make business easier.
					</p>
					<!--p>
						 Made with &nbsp;<i class="icon-heart"></i>&nbsp; by WowThemes.net.
					</p-->
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">F</span>ind <span class="font100">Us</span></h1>
					<div class="footermap">
						<p>
							<strong>Address: </strong>  2010 Corporate Ridge Ste 700
						</p>
                        <p>
							<strong>City/State: </strong>  MacLean, VA 22102
						</p>
						<p>
							<strong>Phone: </strong> + 1 (855) WEBMATION
						</p>
						<p>
							<strong>Fax: </strong> + 1 (855) 531-8825
						</p>
						<p>
							<strong>Email: </strong> <a href="mailto:help@takeweborders.com">info@takeweborders.com</a>
						</p>
						<ul class="social-icons list-soc">
							<li><a href="http://facebook.com/takeweborders" target="_blank"><i class="icon-facebook"></i></a></li>
							<li><a href="http://twitter.com/takeweborders" target="_blank"><i class="icon-twitter"></i></a></li>
							<li><a href="https://www.linkedin.com/company/4982663?trk=prof-exp-company-name" target="_blank"><i class="icon-linkedin"></i></a></li>
							<li><a href="https://plus.google.com/+Takeweborders/posts" target="_blank"><i class="icon-google-plus"></i></a></li>
							<!--li><a href="#"><i class="icon-skype"></i></a></li-->
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">C</span>lients <span class="font100">Praise</span></h1>
					<div id="quotes">
						<div class="textItem">
							<div class="avatar">
								<img src="http://wowthemes.net/demo/biscaya/img/demo/avatar.jpg" alt="avatar">
							</div>
							 "Before turning to those moral and mental aspects of the matter which present the greatest difficulties, let the inquirer begin by mastering more elementary problems.<span style="font-family:arial;">"</span><br/><b> Jesse T, Los Angeles </b>
						</div>
						<div class="textItem">
							<div class="avatar">
								<img src="http://wowthemes.net/demo/biscaya/img/demo/avatar.jpg" alt="avatar">
							</div>
							 "How often have I said to you that when you have eliminated the impossible, whatever remains, however improbable, must be the truth?<span style="font-family:arial;">"</span><br/><b>Ralph G. Flowers </b>
						</div>
					</div>
					<div class="clearfix">
					</div>
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">Q</span>uick <span class="font100">Message</span></h1>
					<div class="done">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">×</button>
							Your message has been sent. Thank you!
						</div>
					</div>
					<form method="post" action="contact.php" id="contactform">
						<div class="form">
							<input class="col-md-6" type="text" name="name" placeholder="Name">
							<input class="col-md-6" type="text" name="email" placeholder="E-mail">
							<textarea class="col-md-12" name="comment" rows="4" placeholder="Message"></textarea>
							<input type="submit" id="submit" class="btn" value="Send">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<p id="back-top">
		<a href="#top"><span></span></a>
	</p>