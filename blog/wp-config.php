<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'twotestblog');

/** MySQL database username */
define('DB_USER', 'twotest');

/** MySQL database password */
define('DB_PASSWORD', '7Jjha3%7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nBPQf&<I}-0qJ}8:?S!qB)BXk.AVNS$+WxIOP;*QXE8c0C+tzAv9!&$*.|quFP[+');
define('SECURE_AUTH_KEY',  '^t<9$jM_YBW2T=c=3h/wT&QD1j$PKL{u*HA@+|~`~PfuquVE6D]ye6wnTirIfOHV');
define('LOGGED_IN_KEY',    '{|{)qELEh/!ElOKeb<PVq?$+[hCq`AKEv+Cy{p vf]?-+<~Mn6K.^&#-8B R.J[u');
define('NONCE_KEY',        '*?{;!N#0.^3]7 +NFxS6OH:,jxoEwZ(O=x2CktoW^,/k+7F^ADo[$IkYM9 s*:+B');
define('AUTH_SALT',        '$WPZDQRCkyCq,Yu8,RYFR10?04/u^H#2uwc7TaHr[aJR+sG6Q[7G763|aAAKlmM@');
define('SECURE_AUTH_SALT', '`43MM*EbWUsGKb?uzui8-O#z|&PLWUs tU/Lh@mNpr|t^-|w[r7HF]l}-Lx`enH,');
define('LOGGED_IN_SALT',   'ek$~8DY-H%A>qRZr)]n!JNV{>9-0l9I6&Om ?HIFg-P5v8Io*#_dqlO>*zh7k6@H');
define('NONCE_SALT',       'q3B_<Q3E@Jb<gw+;MNdV!6 I+bw9_mb}`+&m^,Pit>C c!S+)<x%g,CY?[|Nm&:w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
